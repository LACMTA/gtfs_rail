# Los Angeles County Metropolitan Transportation Authority's GTFS for Rail.
## updated 2025-03-08 01:30:02 PST America/Los_Angeles

As of May 6th, 2016 The LACMTA is now publishing our Bus and Rail Services in separate Google Transit Exports ONLY. As a customer service, and to allow us to update these files more frequently, we have split these files up. The new rail-only export will be updated Daily (generally Tuesday-Saturday mornings), to allow us to send out more timely information and to allow our users to capture all temporary rail service changes that may occur on a daily basis. The bus-only exports will continue to be provided as large-scale changes to the system occur -- generally once every one or two months. We will NOT continue to maintain the combined service feeds.

### Join our developer community at [http://developer.metro.net](http://developer.metro.net) to learn more about using this data.

### Link to LACMTA's Bus Data repository: [https://gitlab.com/LACMTA/gtfs_bus/](https://gitlab.com/LACMTA/gtfs_bus/)

---

### Evergreen link to the gtfs_rail.zip archive: [https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip](https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip?1741426202.7762284)

### Today's link to the gtfs_rail.zip archive: [https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip?1741426202.7762284](https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip?1741426202.7762284)


### zip archive contents
```
 Length   Creation datetime         Name         
------------------------------------------------
     174  2025-03-08 00:10   agency.txt          
    1515  2025-03-08 00:10   calendar.txt        
     248  2025-03-08 00:10   calendar_dates.txt  
     120  2024-07-11 11:40   fare_attributes.txt 
      60  2024-07-11 11:44   fare_rules.txt      
     500  2025-03-08 00:10   routes.txt          
  417954  2024-12-10 12:54   shapes.txt          
   35674  2024-11-12 11:12   stops.txt           
16696582  2025-03-08 00:11   stop_times.txt      
  467735  2025-03-08 00:10   trips.txt           
     285  2025-03-08 00:32   feed_info.txt       
```

## Summary of changes
```
commit 8dbc32c9b5af468de17f247977a859df1e12633e
Author: activebatch <activebatch@MTAABEXEC01>
Date:   Fri Mar 7 01:30:08 2025 -0800

    2025-03-07 01:30:02 PST America/Los_Angeles

 README.md          |    37 +-
 calendar.txt       |    30 +-
 calendar_dates.txt |    15 +
 gtfs_rail.zip      |   Bin 792713 -> 1085525 bytes
 stop_times.txt     | 71564 ++++++++++++++++++++++++++++++++++++++++-----------
 trips.txt          |  3431 ++-
 6 files changed, 58628 insertions(+), 16449 deletions(-)
```

## Subscribing to changes

### Get the latest commit ID with Curl

```
#!/bin/sh

url="https://gitlab.com/LACMTA/gtfs_rail/commits/master.atom"

curl --silent "$url" | grep -E '(title>|updated>)' | \
  sed -n '4,$p' | \
  sed -e 's/<title>//' -e 's/<\/title>//' -e 's/<updated>/   /' \
      -e 's/<\/updated>//' | \
  head -2 | fmt

# returns:
# 2015-12-31T13:09:36Z
#    new info from SPA and instructions on preparing the archive
```

### Get the latest commit ID with Python

```
#!/bin/env python

import feedparser

url = 'https://gitlab.com/LACMTA/gtfs_rail/commits/master.atom'
d = feedparser.parse(url)
lastupdate = d['feed']['updated']

print(lastupdate)

```

See the [http://developer.metro.net/the-basics/policies/terms-and-conditions/](http://developer.metro.net/the-basics/policies/terms-and-conditions/) page for terms and conditions.
