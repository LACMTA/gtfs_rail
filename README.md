# Los Angeles County Metropolitan Transportation Authority's GTFS for Rail.
## updated 2025-03-14 01:30:03 PDT America/Los_Angeles

As of May 6th, 2016 The LACMTA is now publishing our Bus and Rail Services in separate Google Transit Exports ONLY. As a customer service, and to allow us to update these files more frequently, we have split these files up. The new rail-only export will be updated Daily (generally Tuesday-Saturday mornings), to allow us to send out more timely information and to allow our users to capture all temporary rail service changes that may occur on a daily basis. The bus-only exports will continue to be provided as large-scale changes to the system occur -- generally once every one or two months. We will NOT continue to maintain the combined service feeds.

### Join our developer community at [http://developer.metro.net](http://developer.metro.net) to learn more about using this data.

### Link to LACMTA's Bus Data repository: [https://gitlab.com/LACMTA/gtfs_bus/](https://gitlab.com/LACMTA/gtfs_bus/)

---

### Evergreen link to the gtfs_rail.zip archive: [https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip](https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip?1741941003.0895185)

### Today's link to the gtfs_rail.zip archive: [https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip?1741941003.0895185](https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip?1741941003.0895185)


### zip archive contents
```
 Length   Creation datetime         Name         
------------------------------------------------
     174  2025-03-14 00:17   agency.txt          
    1799  2025-03-14 00:17   calendar.txt        
     248  2025-03-14 00:17   calendar_dates.txt  
     120  2024-07-11 12:40   fare_attributes.txt 
      60  2024-07-11 12:44   fare_rules.txt      
     500  2025-03-14 00:17   routes.txt          
  417954  2024-12-10 13:54   shapes.txt          
   35674  2024-11-12 12:12   stops.txt           
23061432  2025-03-14 00:19   stop_times.txt      
  512881  2025-03-14 00:17   trips.txt           
     285  2025-03-14 00:32   feed_info.txt       
```

## Summary of changes
```
commit 3aed8d65851a2b829068e69c2551c2a3a629be93
Author: activebatch <activebatch@MTAABEXEC01>
Date:   Thu Mar 13 01:30:08 2025 -0700

    2025-03-13 01:30:02 PDT America/Los_Angeles

 README.md          |    40 +-
 calendar.txt       |    18 +-
 calendar_dates.txt |     1 -
 gtfs_rail.zip      |   Bin 940774 -> 873616 bytes
 stop_times.txt     | 30929 ++++++++++++++++++---------------------------------
 trips.txt          |  3606 +++---
 6 files changed, 12054 insertions(+), 22540 deletions(-)
```

## Subscribing to changes

### Get the latest commit ID with Curl

```
#!/bin/sh

url="https://gitlab.com/LACMTA/gtfs_rail/commits/master.atom"

curl --silent "$url" | grep -E '(title>|updated>)' | \
  sed -n '4,$p' | \
  sed -e 's/<title>//' -e 's/<\/title>//' -e 's/<updated>/   /' \
      -e 's/<\/updated>//' | \
  head -2 | fmt

# returns:
# 2015-12-31T13:09:36Z
#    new info from SPA and instructions on preparing the archive
```

### Get the latest commit ID with Python

```
#!/bin/env python

import feedparser

url = 'https://gitlab.com/LACMTA/gtfs_rail/commits/master.atom'
d = feedparser.parse(url)
lastupdate = d['feed']['updated']

print(lastupdate)

```

See the [http://developer.metro.net/the-basics/policies/terms-and-conditions/](http://developer.metro.net/the-basics/policies/terms-and-conditions/) page for terms and conditions.
